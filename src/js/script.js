const navMain = document.querySelector('.top-menu');
const burgerBtn = navMain.querySelector('.top-menu__toggle');
const burgerExpanded = navMain.querySelector('[aria-expanded]');

burgerBtn.addEventListener('click', () => {
    if (burgerExpanded) {
        burgerBtn.classList.add('top-menu__toggle--active');
    } else {
        burgerBtn.classList.remove('top-menu__toggle--active');
    }
});

// $(document).ready(function () {
//     $('.material-button-toggle').on("click", function () {
//         $(this).toggleClass('open');
//         $('.option').toggleClass('scale-on');
//     });
// });

/*-------------------   TOOLTIP-----------------------*/
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

/*--------------------- STAR RATING ------------------------*/
let $star_rating = $('.gallery__star-rating .far');

let SetRatingStar = function() {
    return $star_rating.each(function() {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('far').addClass('fas');
        } else {
            return $(this).removeClass('fas').addClass('far');
        }
    });
};

$star_rating.on('click', function() {
    $star_rating.siblings('input.rating-value').val($(this).data('rating'));
    return SetRatingStar();
});

SetRatingStar();
